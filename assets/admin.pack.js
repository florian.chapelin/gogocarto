// Styles
import './scss/admin.scss'

// Javascript
import './js/admin/configuration'
import './js/admin/element-import/element-import'
import './js/admin/osm-tags'
import './js/admin/marker-shape-picker'
import './js/admin/element-edit'
import './js/admin/source-priority'
import './js/admin/charts'
import './js/admin/form-fields/mongo-association-field'


import './js/i18n'
